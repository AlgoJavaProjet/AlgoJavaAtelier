
public class C_Type_Produit {

	protected char id_Type_Produit;
	protected int temps_Traitement;

	public C_Type_Produit(char P_Id_Produit, int P_Temps_Traitement) {

		id_Type_Produit = P_Id_Produit;
		temps_Traitement = P_Temps_Traitement;
	}

	public char get_Id__Type_Produit() {
		return id_Type_Produit;
	}

	public int get_Temps_Traitement() {
		return temps_Traitement;
	}

}

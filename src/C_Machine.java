/**
 * Classe permettant de simuler le fonctionnement d'une machine.
 * @author Roche Romain
 *
 */
public class C_Machine {
	
	protected long Temps_Debut;
	protected long Temps_Fin;
	public static C_Factory Factory = C_Factory.get_Instance();

	public static void main(String[] args) {
		
		//Ajout de différent type de produit.
		
		Factory.ajout_Type_Produit('A', 7);
		Factory.ajout_Type_Produit('B', 5);
		Factory.ajout_Type_Produit('C', 2);
		Factory.ajout_Type_Produit('D', 10);
		
		//Test de la méthode ajout_Type_Produit()
		Factory.ajout_Type_Produit('A', 8);
		Factory.ajout_Type_Produit('E', 8);

		
		//Remplissage du stock de matiere premiere.
		
		Factory.ajout_Matiere_Premiere(new C_Produit(0,Factory.get_Type_Produit(0)));
		Factory.ajout_Matiere_Premiere(new C_Produit(1,Factory.get_Type_Produit(1)));
		Factory.ajout_Matiere_Premiere(new C_Produit(2,Factory.get_Type_Produit(3)));
		Factory.ajout_Matiere_Premiere(new C_Produit(3,Factory.get_Type_Produit(2)));
		Factory.ajout_Matiere_Premiere(new C_Produit(4,Factory.get_Type_Produit(3)));
		Factory.ajout_Matiere_Premiere(new C_Produit(5,Factory.get_Type_Produit(1)));
		Factory.ajout_Matiere_Premiere(new C_Produit(6,Factory.get_Type_Produit(0)));
		Factory.ajout_Matiere_Premiere(new C_Produit(7,Factory.get_Type_Produit(2)));
		Factory.ajout_Matiere_Premiere(new C_Produit(8,Factory.get_Type_Produit(0)));
		Factory.ajout_Matiere_Premiere(new C_Produit(9,Factory.get_Type_Produit(3)));
		
		//Test de la méthode supression_Type_Produit()
		Factory.supression_Type_Produit(0);
		Factory.supression_Type_Produit(4);
		
		//Affiche du stock de matière première.
		
		C_Factory.get_Instance().affichage_Stock_Matiere_Premiere();
		
		//Mise en marche de la machine
		C_Machine machine_1 = new C_Machine();
		
	}
	
	/**
	 * Fonctionnement de la machine
	 */
	public C_Machine() {
		int itaille_Stock_Matiere_Premiere = C_Factory.get_Instance().taille_Stock_Matiere_Premiere();
		
		// Si le stock de mati�re prmi�re n'est pas vide, on traite le stock 
		if(itaille_Stock_Matiere_Premiere > 0 ) {
			Temps_Debut = System.currentTimeMillis()/1000; //Initialisation du temps de traitement du stock par la machine
			
			for(int iindex=0;iindex<itaille_Stock_Matiere_Premiere;iindex++) {
		
				Factory.affichage_Prochain_Produit_Usine();
		
				Traitement(Factory.get_Prochain_Produit_Usine().get_Type_Produit().get_Temps_Traitement());
		
				Factory.ajout_Produit_Fini(Factory.get_Prochain_Produit_Usine());
				Factory.supression_Matiere_Premiere();
			}
		
			Temps_Fin = System.currentTimeMillis()/1000; //Fin du traitement du stock par la machine
			
			Factory.affichage_Stock_Produit_Fini();
			
			Affichage_Temps_Simulation();
		}
		else System.out.println("Erreur, le stock de mati�re premi�re est vide.");
	}
	
	/**Traitement du produits dans la machine
	 * 
	 * @param P_Temps_Traitement: Temps donn� par un type de produit pour �tre trait�
	 */
	public void Traitement(int P_Temps_Traitement) {
		
		while(P_Temps_Traitement>0){
			P_Temps_Traitement--;
		}
	}
	
	/**
	 * Calcule du temps de simulation en HH:MM:SS
	 */
	public void Affichage_Temps_Simulation() {
		
		int iheure = (int) ((Temps_Fin-Temps_Debut) / 3600);
        int iminute = (int) (((Temps_Fin-Temps_Debut) % 3600) / 60);
        int iseconde = (int) ((Temps_Fin-Temps_Debut) % 60);
		
		System.out.println("\nTemps de simulation: "+iheure+":"+iminute+":"+iseconde);
		
	}
	
}

import java.text.DateFormat;
import java.util.Date;

/**
 * Classe repr�sentant un produit.
 * @author Roche Romain
 *
 */

public class C_Produit {

	/**
	 * repr�sente l'identifiant unique du produit.
	 */
	protected final int id_Produit;
	/**
	 * repr�sente le type du produit.
	 */
	protected C_Type_Produit type_Produit;
	/**
	 * repr�sente la date de traitement du produit.
	 */
	protected Date date;
	
	/**
	 * Constructeur de la classe C_Produit
	 * @param P_Id_Produit Param�tre donnant l'identifiant unique du produit.
	 * @param P_Type_Produit Param�tre donnant le type du produit.
	 */
	public C_Produit(int P_Id_Produit,C_Type_Produit P_Type_Produit) {
		id_Produit = P_Id_Produit;
		type_Produit = P_Type_Produit;
		date = null;
	}
	
	/**
	 * M�thode permettant d'attribuer une date � un produit.
	 * @param P_Date Param�tre de de type date.
	 */
	public void set_Date(Date P_Date) {
		date = P_Date;
	}
	
	/**
	 * M�thode permettant de r�cup�rer la date d'un produit.
	 * @return Retourne l'attribut date du produit.
	 */
	public Date get_Date() {
		return date;
	}
	
	/**
	 * M�thode permettant de r�cup�rer l'identifiant unique d'un produit.
	 * @return Retourne l'identifiant du produit.
	 */
	public int get_Id_Produit() {
		return id_Produit;
	}
	
	/**
	 * M�thode permettant de r�cup�rer le type du produit.
	 * @return Retourne le type du produit.
	 */
	public C_Type_Produit get_Type_Produit() {
		return type_Produit;
	}
	
	/**
	 * Affiche les caract�ristiques du produit.
	 */
	public void Affiche_Produit() {
		if(date != null) {
			DateFormat l_format_Date = DateFormat.getDateTimeInstance(DateFormat.MEDIUM,DateFormat.MEDIUM);
			System.out.println("Id_Produit: " + id_Produit + "		Type_Produit: " + type_Produit.get_Id__Type_Produit() + "		" + l_format_Date.format(date));
		}
		else System.out.println("Id_Produit: " + id_Produit + "		Type_Produit: " + type_Produit.get_Id__Type_Produit() );
	}
}

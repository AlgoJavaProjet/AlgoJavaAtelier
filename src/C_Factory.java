import java.util.Date;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Classe singletont regroupant les différentes listes ainsi que les méthodes nécéssaires à la simulation des stocks et leurs traitements.
 * @author Roche
 *
 */
public class C_Factory {

	/**
	 * Représent le stock de matière première en amont de la machine.
	 */
	Queue<C_Produit> stock_Matiere_Premiere;
	/**
	 * Représent le stock de produit finie après le traitement de la machine.
	 */
	Queue<C_Produit> stock_Produit_Fini;
	/**
	 * Liste contenant les type de produit.
	 */
	ArrayList<C_Type_Produit> enumeration_Type_Produit;
	/**
	 * Initialisaton de l'instance du singleton.
	 */
	protected static C_Factory INSTANCE = new C_Factory();
	
	/**
	 * Constructeur de C_factory initialisant les différentes liste.
	 */
	protected C_Factory() {
		
		stock_Matiere_Premiere = new LinkedList<C_Produit>();
		stock_Produit_Fini = new LinkedList<C_Produit>();
		enumeration_Type_Produit = new ArrayList<C_Type_Produit>();

	}
	
	/**
	 * Méthode permettant d'ajouter des types de produit
	 * @param P_Id_Produit Représente l'identifiant unique d'un type de produit.
	 * @param P_Temps_Traitement Représente le temps de traitement d'un type de produit.
	 */
	public void ajout_Type_Produit(char P_Id_Produit,int P_Temps_Traitement) {
		int iidex=0;
		boolean bid_Produit_Valide = true; //On part du principe que le type de produit à ajouter n'existe pas dans la liste.
		while ((iidex<enumeration_Type_Produit.size()) & bid_Produit_Valide){ //On cherche si le type de produit existe déja.
			if (enumeration_Type_Produit.get(iidex).get_Id__Type_Produit() == P_Id_Produit) {
				System.out.println("Ce type de produit existe déja.");
				bid_Produit_Valide = false;
			}
			iidex++;
		}
		if(bid_Produit_Valide){ //Si le type de produit n'existe pas, on le rajoute.
			enumeration_Type_Produit.add(new C_Type_Produit(P_Id_Produit, P_Temps_Traitement));
			System.out.println("Ajout du type de produit Id_Produit: "+P_Id_Produit);
		}
	}
	
	/**
	 * Méthode permettant de suprimer un type de produit à une position donnée.
	 * @param P_Index Position du type de produit à suprimer.
	 */
	public void supression_Type_Produit(int P_Index){
		boolean btype_Produit_Lie_A_Un_Produit = false;
		for(C_Produit index_Produit : stock_Matiere_Premiere ) { //Recherche si le type de produit est rataché à un produit.
			if(!btype_Produit_Lie_A_Un_Produit) {
				if(index_Produit.get_Type_Produit() == enumeration_Type_Produit.get(P_Index)) {
					System.out.println("Erreur, ce type de produit ne peut être suprimer car il a déja été rataché à un produit.");
					btype_Produit_Lie_A_Un_Produit = true;
				}
			}
		}
		if(!btype_Produit_Lie_A_Un_Produit){ //Si le type de produit n'est pas rataché à un produit on peut le suprimer.
			enumeration_Type_Produit.remove(P_Index);
			System.out.println("Supression du type de produit situé à l'index: " + P_Index);
		}
	}
	
	/**
	 * Méthode permettant de récupérer l'instance de C_Factory.
	 * @return Retourne l'instance de C_Factory.
	 */
	public static C_Factory get_Instance() {
		return INSTANCE;
	}
	
	/**
	 * Méthode permettant de récupérer le type du produit à l'index voulu  
	 * @param P_Index  Position du produit dans le stock.
	 * @return Retourne le type du produit à la position donnée.
	 */
	public C_Type_Produit get_Type_Produit(int P_Index) {
		return enumeration_Type_Produit.get(P_Index);
	}
	
	/**
	 * Méthode permettant de récuperer le prochain produit à être usiné.
	 * @return Reroune le produit qui va être usiné.
	 */
	public C_Produit get_Prochain_Produit_Usine() {
		return stock_Matiere_Premiere.element();
	}
	
	/**
	 * Méthode permettant d'ajouter un produit au stock de matière première.
	 * @param P_Produit Produit à ajouter au stock de matière première.
	 */
	public void ajout_Matiere_Premiere(C_Produit P_Produit) {
		stock_Matiere_Premiere.add(P_Produit);
	}
	
	/**
	 * Méthode permettant d'ajouter un produit dans le stock de produit fini.
	 * @param P_Produit Produit à ajouter au stock de produit fini.
	 */
	public void ajout_Produit_Fini(C_Produit P_Produit) {
		P_Produit.set_Date(new Date()); //Ajout de la date de fin de traitement du produit.
		stock_Produit_Fini.add(P_Produit);
	}
	
	/**
	 * Méthode permettant de suprimer le produit qui a ètè usiné.
	 */
	public void supression_Matiere_Premiere() {
		if(taille_Stock_Matiere_Premiere() > 0) {
			stock_Matiere_Premiere.remove();
		}
	}
	
	/**
	 * Méthode permettant de récupérer la taille du stock de matière première.
	 * @return Retourne la taille du stock de matière première.
	 */
	public int taille_Stock_Matiere_Premiere() {
		return stock_Matiere_Premiere.size();
	}
	
	/**
	 * Méthode affichant la taille du stock de matière première.
	 */
	public void affichage_taille_Stock_Matiere_Premiere() {
		System.out.println("Taille du Stock des matières premi�res: " + taille_Stock_Matiere_Premiere());
	}
	
	/**
	 * Méthode permettant de récupérer la taille du stock de produit finie.
	 * @return Retourne la taille du stock de produit fini.
	 */
	public int taille_Stock_Produit_Fini() {
		return stock_Produit_Fini.size();
	}
	
	/**
	 * Méthode affichant la taille du stock de produit finie.
	 */
	public void affichage_taille_Stock_Produit_Fini() {
		System.out.println("Taille du Stock des produits finies: " + taille_Stock_Produit_Fini());
	}
	
	/**
	 * Méthode permettant d'afficher l'intégralité du stock de matière première.
	 */
	public void affichage_Stock_Matiere_Premiere() {
		if(taille_Stock_Matiere_Premiere()>0) {
			System.out.println("Affichage du stock des matières premières:");
			for(C_Produit l_Index_Produit : stock_Matiere_Premiere) {
				l_Index_Produit.Affiche_Produit();
			}
		}
	}
	
	/**
	 * Méthode permettant d'afficher l'intégralité du stock de produit finie.
	 */
	public void affichage_Stock_Produit_Fini() {
		if(taille_Stock_Produit_Fini()>0) {
			System.out.println("Affichage du stock des produits finies:");
			for(C_Produit l_Index_Produit : stock_Produit_Fini) {
				l_Index_Produit.Affiche_Produit();
			}
		}
	}
	
	/**
	 * Méthode affichant le prochain produit à être usiné.
	 */
	public void affichage_Prochain_Produit_Usine() {
		if(taille_Stock_Matiere_Premiere() > 0) {
			System.out.println("Prochain produit à être usiné:");
			stock_Matiere_Premiere.element().Affiche_Produit();
		}
	}
}
